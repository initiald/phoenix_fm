import 'package:flutter/material.dart';

AppBar headerNav() {
  return AppBar(
    automaticallyImplyLeading: false,
    flexibleSpace: Image(
      image: AssetImage("images/bg.png"),
      fit: BoxFit.cover,
    ),
    title: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      // crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Flexible(
          child: Container(
            height: 35.0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
            ),
          ),
          flex: 1,
        ),
        Flexible(
          child: Image.asset(
            "images/header_text.png",
            fit: BoxFit.contain,
            height: 100,
          ),
          // flex: 1,
        ),
        Flexible(
          child: Container(
            height: 35.0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    bottomLeft: Radius.circular(10.0)),
              ),
            ),
          ),
          flex: 1,
        ),
      ],
    ),
  );
}
