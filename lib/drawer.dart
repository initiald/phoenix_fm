import 'package:flutter/material.dart';

import 'live_chart.dart';

Drawer drawer(context) {
  var menus = ['PLAYER', 'LIVE CHARTS', 'CONTACT', 'GALLERY', 'EVENTS'];
  return Drawer(
    child: Container(
      color: Colors.grey[900],
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            height: 150,
            child: DrawerHeader(
              child: Row(
                children: [
                  Image.asset(
                    "images/logo.png",
                    fit: BoxFit.contain,
                    height: 60,
                  ),
                  Column(
                    children: [
                      Text(
                        "MENU",
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "SELECTOR",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: List.generate(
                5,
                (index) => ListTile(
                  title: Row(
                    children: [
                      Image.asset(
                        "images/btn_play.png",
                        fit: BoxFit.contain,
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          menus[index],
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Update the state of the app.
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LiveChart(),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
