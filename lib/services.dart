import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:phoenix_fm/model/get_prod.dart';
import 'package:phoenix_fm/model/model_podcast.dart';
import 'package:phoenix_fm/model/model_program.dart';

class servicewrapper {
  var api_phoenix = "https://phoenixradiobali.com/jsonPhoenix/";
  var baseurl = "https://androidtutorial.blueappsoftware.com/";
  var apifolder = "webapi/";
// see complete url --
// http://androidtutorial.blueappsoftware.com/webapi/get_jsondata.php
  // advance usage
  Future<get_prod> getProdCall() async {
    var url = baseurl + apifolder + "get_jsondata.php";
    final body = {'language': 'default', 'securecode': '123'};
    final bodyjson = json.encode(body);
    // pass headers parameters if any
    final response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: bodyjson);
    print(" url call from " + url);
    if (response.statusCode == 200) {
      print('url hit successful' + response.body);
      String data = response.body;
      print(' prod name - ' + jsonDecode(data)['Information'][0]['name']);
      return get_prod.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  Future<ModelProgram> getProgram(String hari) async {
    var url = api_phoenix + "program.php";
    var vbody = {'hari': hari};
    var response = await http.post(url, body: vbody);
    // print('Response status: ${response.statusCode}');
    // print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      return ModelProgram.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }
  }

  // simple usage
  Future<ModelPodcast> getPodcast() async {
    var response = await http.get(api_phoenix + "podcast.php");
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      print('url hit successful' + response.body);
      String data = response.body;
      // print(' prod name - ' + jsonDecode(data)['Information'][0]['name']);
      print(' res - ' + jsonDecode(data));
      return ModelPodcast.fromJson(json.decode(response.body));
    } else {
      print('failed to get data');
      throw Exception('Failed to get data');
    }

    // print(await http.read('https://example.com/foobar.txt'));
  }
}
