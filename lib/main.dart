import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:intl/intl.dart';
import 'package:phoenix_fm/appBar.dart';
import 'package:phoenix_fm/drawer.dart';
import 'package:phoenix_fm/live_chart.dart';
import 'package:phoenix_fm/model/model_program.dart';
import 'package:phoenix_fm/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Phoenix FM',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final globalKey = GlobalKey<ScaffoldState>();
  // static const streamUrl = "http://54.179.9.11:8350/;stream.mp3"; //pinguin
  static const streamUrl = "http://50.97.122.58:8250/;stream.mp3"; //phoenix
  // static const streamUrl = "http://live.phoenixradiobali.com";
  bool isPlaying = false;

  Future<ModelProgram> getModelProgram;
  String _timeString;
  List _myPage = [
    // Player(),
    LiveChart(),
  ];

  void initState() {
    super.initState();
    audioStart();
    playingStatus();
    // FlutterRadio.playOrPause(url: streamUrl);
    getModelProgram = _getDatacall();
    if (_timeString == null) {
      _getTime();
    }
    Timer.periodic(Duration(minutes: 1), (Timer t) => _getTime());
  }

  void _getTime() {
    final String formattedDateTime =
        // DateFormat('yyyy-MM-dd \n kk:mm:ss').format(DateTime.now()).toString();
        DateFormat('kk:mm').format(DateTime.now());
    setState(() {
      _timeString = formattedDateTime;
      // print(_timeString);
    });
  }

  Future<void> audioStart() async {
    await FlutterRadio.audioStart();
    print('Audio Start OK');
  }

  @override
  Widget build(BuildContext context) {
    // var menus = ['PLAYER', 'LIVE CHARTS', 'CONTACT', 'GALLERY', 'EVENTS'];
    return Scaffold(
      key: globalKey,
      appBar: headerNav(),
      // body: PageView.builder(
      //   itemBuilder: (context, position) => _myPage[position],
      //   itemCount: _myPage.length,
      // ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/bg.png"), fit: BoxFit.cover),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.all(17),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Center(
                        Image.asset(
                          "images/dumy_img.png",
                          fit: BoxFit.contain,
                          // height: 35,
                        ),
                        // ),
                      ],
                    ),
                  ),
                  Column(
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      // Column(
                      //   children: [
                      // Center(
                      FutureBuilder<ModelProgram>(
                        future:
                            getModelProgram, // here get_datacall()  can be call directly
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            var program = snapshot.data.program;
                            // print(program.length);
                            int currentProgram = 0;
                            for (var i = 0; i < program.length; i++) {
                              var startTime = program[i].mulai.split(":");
                              var endTime = program[i].akhir.split(":");
                              int startToMinutes =
                                  int.parse(startTime[0]) * 60 +
                                      int.parse(startTime[1]);
                              int endToMinutes = int.parse(endTime[0]) * 60 +
                                  int.parse(endTime[1]);
                              var currentTime = _timeString.split(":");
                              int ctimeToMinutes =
                                  int.parse(currentTime[0]) * 60 +
                                      int.parse(currentTime[1]);

                              if (startToMinutes <= ctimeToMinutes) {
                                currentProgram = i;
                              }
                            }

                            return Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      program[currentProgram].acara,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 25.0),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      program[currentProgram].host,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 22.0),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      program[currentProgram].mulai,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20.0),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          } else if (snapshot.hasError) {
                            return Text(
                              "error",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20.0),
                            );
                          }
                          // By default, show a loading spinner
                          return Container(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator(),
                          );
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        child: Container(
          decoration: BoxDecoration(color: Colors.grey[900]),
          padding: EdgeInsets.all(10),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // Scaffold.of(context).openDrawer();
                      // final snackBar = SnackBar(content: Text('Profile saved'));
                      globalKey.currentState.openDrawer();
                    },
                    child: Image.asset(
                      "images/btn_menu.png",
                      fit: BoxFit.contain,
                      height: 35,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () {
                      // get data and assign to model class
                      // getModelProgram = _getDatacall();
                      // update the UI
                      // setState(() {});
                      playingStatus();
                      FlutterRadio.playOrPause(url: streamUrl);
                    },
                    child: Image.asset(
                      isPlaying
                          ? "images/btn_play.png"
                          : "images/btn_pause.png",
                      fit: BoxFit.contain,
                      height: 55,
                    ),
                  ),
                  // child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
      drawer: drawer(context),
    );
  }

  Future playingStatus() async {
    bool isP = await FlutterRadio.isPlaying();
    setState(() {
      isPlaying = isP;
      print('is playing $isPlaying');
    });
  }

  Future<ModelProgram> _getDatacall() {
    print(" get data using http");
    // call servicewrapper function
    servicewrapper wrapper = new servicewrapper();
    return wrapper.getProgram('2');
    //get_prodlist
    // To display the data on screen, use the FutureBuilder widget.
    // The FutureBuilder widget comes with Flutter and makes it easy to work with asynchronous data sources
    // The Future you want to work with. In this case, the future returned from the  getProdCall() function.
    //A builder function that tells Flutter what to render, depending on the state of the Future: loading, success, or error.
  }
}
